//1
function maxElement(arr){
arr.sort((prev,next) => prev-next);
return Math.max(arr[0],arr[arr.length-1]);
}
//

//2
function copyArray(array){
    const new_array=[];
    array.forEach(element => {
        new_array.push(element);
    });
    
    return new_array;
}

//3
function addUniquaiedId(obj){
let id=Symbol('id');
obj[id]=123;
}
//4


function regroupObject(obj){
const new_obj={}
let {name,details}=obj;
let details_keys_arr=Object.keys(details);
let last_element=details_keys_arr[details_keys_arr.length-1];
new_obj[last_element]=details[last_element];
new_obj.user={}
new_obj.user.name=name;
for(let q=0; q<details_keys_arr.length-1;q++){
    new_obj.user[details_keys_arr[q]]=details[details_keys_arr[q]];
}
return new_obj;
}

//5
function findUniqueElementd(array){

    let my_collection=new Set();
    let result=[]

    array.forEach((element) => {
        my_collection.add(element);
    })
    console.log(my_collection.keys)

    for (let item of my_collection.keys()){
        result.push(item);
    }
    

    return result;
}

//6


function hideNumber(number){
    let result='';
    number=String(number);
    let last_four_numbers=/\d{4}$/gmi;
    console.log(number.match(last_four_numbers));

    for(let q=0;q<number.length-4;q++){
        result+='*'
    }
    result+=`${number.match(last_four_numbers)}`;
   return result;
}

//7
function add(a,b){
let result=a+b;


if(isNaN(result)){
    throw new Error('Missing property');
}else{
return result;
}
}

//8
function getRes(transform){
    if(transform){
let result=[]
    transform.forEach(element => {
        result.push(element.name)
    });
    result.sort();
    console.log(result);
    return result;
} else{
    return fetch('https://jsonplaceholder.typicode.com/users')
}
}

getRes()
.then(response => response.json())
.then(json => getRes(json));

//9
async function getResTransform(){
const result= [];


    const response= await fetch('https://jsonplaceholder.typicode.com/users');
    const data=await response.json()

    data.forEach((e) => {
        result.push(e.name)
    });
    result.sort();
    return result;
    
}

getResTransform()