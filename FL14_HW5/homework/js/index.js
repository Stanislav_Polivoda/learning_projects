// Your code goes here

let posts_counter=0;

const container=document.querySelector('.table');
container.insertAdjacentHTML('beforeend',
`<tr class="row"><td>.id</td>
<td>Name</td>
<td>Username</td>
<td>Email</td>
</tr>`)



fetch('https://jsonplaceholder.typicode.com/users')
  .then(response => response.json())
  .then(jsont => getUsers(jsont))





  function getUsers(arr){

const arr_of_users=arr;

for(let obj of arr_of_users){


  let id_td=document.createElement('td');
  let name_td=document.createElement('td');
  let username_td=document.createElement('td');
  let email_td=document.createElement('td');
  const tr=document.createElement('tr');
  const button=document.createElement('button');
  const delete_button=document.createElement('button');
  button.textContent='edit';
  delete_button.textContent='delete' 

  id_td.textContent=`${obj.id}`;
  name_td.textContent=`${obj.name}`;
  username_td.textContent=`${obj.username}`;
  email_td.textContent=`${obj.email}`;
  tr.appendChild(id_td);
  tr.appendChild(name_td)
  tr.appendChild(username_td)
  tr.appendChild(email_td)
  tr.appendChild(button);
  tr.appendChild(delete_button);

  container.appendChild(tr);


  
button.addEventListener('click',() => {

let input_name=document.createElement('input');
let input_username=document.createElement('input');
let input_email=document.createElement('input');
let new_button=document.createElement('button');

input_name.setAttribute('value',`${obj.name}`)
input_username.setAttribute('value',`${obj.username}`)
input_email.setAttribute('value',`${obj.email}`)
new_button.textContent='save changes'


  tr.replaceChild(input_name,name_td)
  tr.replaceChild(input_username,username_td)
  tr.replaceChild(input_email,email_td)
  tr.replaceChild(new_button,button)

  new_button.addEventListener('click',() => {



    name_td.textContent=`${input_name.value}`;
    username_td.textContent=`${input_username.value}`;
    email_td.textContent=`${input_email.value}`;


    tr.replaceChild(name_td,input_name)
    tr.replaceChild(username_td,input_username)
    tr.replaceChild(email_td,input_email)
    tr.replaceChild(button,new_button)


    fetch(`https://jsonplaceholder.typicode.com/users/${obj.id}`, {
  method: 'PUT',
  body: JSON.stringify({
    email: `${input_email.value}`,
    id: `${obj.id}`,
    name: `${input_name.value}`,
    username: `${input_username.value}`
  }),
  headers: {
    'Content-type': 'application/json; charset=UTF-8'
  }
})
  .then((response) => response.json())
  .then((json) => json)



  })

})

delete_button.addEventListener('click',() => {
  tr.remove()
  fetch(`https://jsonplaceholder.typicode.com/users/${obj.id}`, {
  method: 'DELETE'
})
})




name_td.addEventListener('click',() => {
fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => response.json())
.then(json => show_posts(json,tr,obj.id))
.then(() => fetch('https://jsonplaceholder.typicode.com/comments'))
.then(response => response.json())
.then(json => show_comments(json,tr,obj.id))
})
}


  }

  function show_posts(value,container,id){



    const q=document.querySelector('.posts-container');
  

    let container_posts=document.createElement('tbody');
    container_posts.classList.add('posts-container');
    container.insertAdjacentElement('afterend',container_posts)

    let users_posts=value.filter(obj => obj.userId===id);

    if(q===null){
      for(let q of users_posts ){
        container_posts.insertAdjacentHTML('afterbegin',
        `<tr class="posts">
        <td>${q.id}</td>
        <td>${q.title}</td>
        <td>${q.body}</td>
        </tr>`)
      } 
    } else {
      container_posts.remove();
      q.remove()
    }




}

  


  function show_comments(value,container,id){
    const q=document.querySelector('.comments-container');
  

    let container_comments=document.createElement('tbody');
    container_comments.classList.add('comments-container');
    container.insertAdjacentElement('afterend',container_comments)
  
    let users_comments=value.filter(obj => obj.postId===id);
  


    if(q===null){
      for(let q of users_comments ){
        container_comments.insertAdjacentHTML('afterbegin',
        `<tr class="comments">
        <td>${q.id}</td>
        <td>${q.name}</td>
        <td>${q.body}</td>
        <td>${q.email}</td>
        </tr>`)
      } 
    } else {
      container_comments.remove();
      q.remove()
    }




    
  }

  







  


