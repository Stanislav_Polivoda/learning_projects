// Your code goes here

let needArr=[];
let suit_arr= ['Hearts', 'Diamonds', 'Clubs', 'Spades'];

let qw_1={
    number:11,
    card_suit:'Spades'
}

for(let q of suit_arr){
for(let i=1; i<14; i++){
    needArr.push({
        number:i,
        card_suit:q
    })
}
}

class Deck{
    constructor(array){
        this.array=array;
        this.count=array.length;
    }
 

    shuffle() {
       return this.array.sort(() => Math.random() - 0.5);
      }
    draw(n){
        this.array.splice(this.array.length-n,n);
      
       return this.array.slice(this.array.length-n,this.array.length);
    }
}


let desk=new Deck(needArr);



class Card{
    constructor(card){
    this.suit=card.card_suit;
    this.rank=card.number;
    //this.isFaceCard=this.rank;
    }

    get isFaceCard(){
        if(this.rank===1||this.rank>10){
           return true;
        } else{
            return false;
        } 
       }



    toString(){
let rank_obj={
    1: 'Ace',
     11: 'Jack',
      12:'Queen',
    13: 'King'};

let res=this.rank;
let result;
if(res===1||res>10){
    result=true;
} else{
    result=false;
 } 
if(result){
return ` ${rank_obj[this.rank]} of ${this.suit}`
} else{
    console.log(this.isFaceCard);
    return `${this.rank} of ${this.suit}`
}
    }

    compare(arr){
        if(arr.length>0){
let cardOne=arr[0];
let cardTwo=arr[1];

if(cardOne.number-cardTwo.number>0){

   return {
       first_card_result:1,
       second_card_result:0
    };

}
if(cardOne.number-cardTwo.number<0){
 
    return {
        first_card_result:0,
        second_card_result:1
     };
} else {
    return {
        first_card_result:0,
        second_card_result:0    
    }
}
        } else{
 
    return {
        first_card_result:0,
        second_card_result:0 
    }
}
    }
 

   

}


class Player{
    constructor(name,wins,deck){
this.name=name;
this.wins=wins;
this.deck=deck;
    }
    play(){
start_game();
    }
}

let first_player= new Player('Stas',0,needArr);
let second_player=new Player('Sasha',0,needArr);
first_player.play();




function start_game(){
    let game_rezult=[];
    let result_first_player=0;
    let result_second_player=0;
    desk.shuffle();
    while(needArr.length>0){
        let first_card=new Card(qw_1,null);
        game_rezult.push(first_card.compare(desk.draw(2)));       
        
    
    }

  

    for(let q of game_rezult){
        result_first_player+=q.first_card_result;
        result_second_player+=q.second_card_result;
    }


    if(result_first_player>result_second_player){
        alert(`firstPlayer wins ${result_first_player} to ${result_second_player}`);
    }
    if(result_first_player<result_second_player){
        alert(`secondPlayer wins ${result_second_player} to ${result_first_player}`)
    }
}


// task 2


class Employee{
    

  
    constructor(id, 
        firstName, 
        lastName, 
       birthday, 
        salary, 
        position, 
        department){
     this.id=id;
     this.firstName=firstName;
     this.lastName =lastName;
     this.birthday=birthday;
     this.salary=salary;
     this.position=position;
     this.department=department;
     this._age; //
     this._fullName;
    
     
    }




    get age(){
        let date=new Date();
        let year=date.getFullYear();
        let month=date.getMonth();
        let day=date.getDate();

        let birthday_day=this.birthday.slice(0,2);
        let birthday_month=this.birthday.slice(3,5);
        let birthday_year=this.birthday.slice(6,10);

        let age=0;
        
        if(Number(day)-Number(birthday_day)<0){
            month-1;
        }
        if(month<birthday_month){
            birthday_year-1;
        }
        age=year-birthday_year;
       return age;


    }
    get fullName(){
     let _result=`${this.firstName} ${this.lastName}`;
     return _result;
    }


    changeDepartment(newDepartment){
        this.department=newDepartment;
    }
    changePosition(newPosition){
        this.position=newPosition;
    }
    changeSalary(newSalary){
        this.salary=newSalary;
    }
    getPromoted(benefits){
        this.salary=benefits.salary;
        this.position=benefits.position;
        this.department=benefits.department;
    }
    getDemoted(punishment){
        this.salary=punishment.salary;
        this.position=punishment.position;
        this.department=punishment.department;    
    }   
} 



let stas_polo=new Employee(11,'Stas','Polivoda','11.08.2000',100500,'developer','dev');
let stas_polot=new Employee(112,'Stas','Polivoda','11.08.2000',100500,'developer','dev');
stas_polo.changePosition('CEO');





class Manager extends Employee {
    constructor(id, 
        firstName, 
        lastName, 
       birthday, 
        salary, 
        position, 
        department){
            super(id, 
                firstName, 
                lastName, 
               birthday, 
                salary, 
                department)
        this.position='manager' 
            }

} 

class BlueCollarWorker extends Employee{}


class HRManager extends Manager{
    constructor(id, 
        firstName, 
        lastName, 
       birthday, 
        salary, 
        position 
        ){
            super(id, firstName, lastName, birthday, salary, position)
        this.department='hr' 
            }
}


 class SalesManage extends Employee{
    constructor(id, 
        firstName, 
        lastName, 
       birthday, 
        salary, 
        position 
        ){
            super(id, firstName, lastName, birthday, salary,position )
        this.department='sales' 
            }
 }
